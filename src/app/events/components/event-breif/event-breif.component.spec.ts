import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventBreifComponent } from './event-breif.component';

describe('EventBreifComponent', () => {
  let component: EventBreifComponent;
  let fixture: ComponentFixture<EventBreifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventBreifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventBreifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
